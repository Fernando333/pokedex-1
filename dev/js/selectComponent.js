var numeral = require('numeral');

var SelectComponent = function(selector){
	console.debug('Loading SelectComponent with selector '+selector);
	this.selector=selector;
	this.element = document.getElementById(selector.replace('#',''));
	
}

SelectComponent.prototype.fill = function(collection){
	console.debug('SelectComponent::method(fill) for '+this.selector);
	//console.info(this)
	this.clean();
	var self= this;
	collection.forEach(function(item,index){
		self.element.options.add(new Option(item.text,item.value))
	});
	this.element.value=collection[0].value;
	
}


SelectComponent.prototype.selectedText = function(){
	if (this.element.selectedIndex != -1){
		return this.element.options[this.element.selectedIndex].text;
	}
}

SelectComponent.prototype.selectedValue = function(){
	if (this.element.selectedIndex != -1){
		return this.element.options[this.element.selectedIndex].value;
	}
}

SelectComponent.prototype.clean = function(){
	while(this.element.length>0 ){
		this.element.remove(0);
	}
}


SelectComponent.prototype.fillColonias = function(collection){
	this.clean();
	var self= this;
	collection.forEach(function(item,index){
		self.element.options.add(new Option(item.neighborhood.name.toUpperCase()))
	})
}

SelectComponent.prototype.selectTexto  = function (text){
	$(this.selector).val(text)
}

module.exports = SelectComponent; 