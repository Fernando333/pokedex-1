var Contenedor = function (selector){
	this.selector = selector ;
	this.element = document.getElementById(selector.replace("#",""));
}

Contenedor.prototype.hideFrame = function(){
	$(this.selector).hide() ;
}
Contenedor.prototype.showFrame = function(){
	$(this.selector).show();
}
Contenedor.prototype.getElement = function(){
	return this.element;
}

module.exports = Contenedor
