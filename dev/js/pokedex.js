var Contenedor					= require('./contenedor');
var SelectComponent				= require('./selectComponent');
var Servicios					= require('./Servicios');
var Label						= require('./label');
var Pokedex = function(){

	this.contenedor1 = new Contenedor("#collapse1");
	this.contenedor2 = new Contenedor("#collapse2");
	this.busqueda	 = new Contenedor("#busqueda");

	this.servicios 	= new Servicios();

	this.buttonReturn		= new SelectComponent("#return");
	this.buttonclose		= new SelectComponent("#close");
	this.especies 			= new SelectComponent("#especies");
	this.searchPkemon		= new SelectComponent("#searchPkemon");
	this.btnBuscarPokemon	= new SelectComponent("#btnBuscarPokemon");
	this.btnBuscarNextPokemon	= new SelectComponent("#next");
	this.btnBuscarPrevPokemon	= new SelectComponent("#prev");

	this.idPokemon			= new Label("#idPokemon");
	this.namePokemon		= new Label("#namePokemon");
	this.typePokemon		= new Label("#typePokemon");
	this.heightPokemon		= new Label("#heightPokemon");
	this.weightPokemon		= new Label("#weightPokemon");
	this.descripcion		= new Label("#descripcion");


	this.nombrePokemon	= document.getElementById("nombrePokemon");

	this.buttonReturn.element.addEventListener("click",this.showColapse.bind(this));
	this.buttonclose.element.addEventListener("click",this.closeColapse.bind());
	this.searchPkemon.element.addEventListener("click",this.btnSearchPokemon.bind(this));
	this.btnBuscarNextPokemon.element.addEventListener("click",this.searchNextPokemon.bind(this));
	this.btnBuscarPrevPokemon.element.addEventListener("click",this.searchPrevPokemon.bind(this));


	$("#collapse1").hide();
	$("#collapse2").show();
	$("#busqueda").show();
	$("#especiesList").show();
	$("#infoPokemon").hide();
	$("#imgPkemon").hide();
	$("#descriptionDiv").hide();
	$("#buttonsTop").hide();
	
	
	//this.especies.element.addEventListener("change",this.changeEspecies.bind(this));

	this.datos = [];

	this.servicios.todos()
	.then(this.imprimeTodos.bind(this));

	this.buttonclose.element.disabled=true

}

Pokedex.prototype.imprimeTodos=function(data){
	console.log("data::");
	console.log(data);

}


Pokedex.prototype.btnSearchPokemon=function(){
	$("#especiesList").hide();
	$("#imgPkemon").show();
	$("#infoPokemon").show();
	$("#descriptionDiv").show();
	this.buttonclose.element.disabled=false

	var name = this.nombrePokemon.value;
	this.servicios.nombrePokemon(name)
	.then(this.paintInfo.bind(this));

}

Pokedex.prototype.searchNextPokemon=function(){

	var idActual = this.dataInfoPokemon.id
	var idNext= idActual+1

	this.servicios.idPokemon(idNext)
	.then(this.paintInfo.bind(this));

}

Pokedex.prototype.searchPrevPokemon=function(){



	var idActual = this.dataInfoPokemon.id
	var idPrev= idActual-1

	this.servicios.idPokemon(idPrev)
	.then(this.paintInfo.bind(this));

}

Pokedex.prototype.paintInfo=function(data){
	console.log("Info del pokemon "+data.name)
	console.log(data)
	var self = this;

	this.servicios.caracteristicPokemon(data.id)
	.then(this.paintDescription.bind(this))

	this.dataInfoPokemon= data;

	$('#imagenPokemon').html("");
	$('#typePokemon').html("");
	$('#especies').html("")


	this.idPokemon.setValue(data.id);	
	this.namePokemon.setValue(data.name);
	this.heightPokemon.setValue(data.height);		
	this.weightPokemon.setValue(data.weight+" lbs");

	data.types.forEach(function(item,idx){

		var span = "<span id="+item.type.name+"  class='badge'>"+item.type.name+"</span>"
		$('#typePokemon').append(span)

		self.asignaColor(item.type.name)
		
	});


	var image = new Image();
    var src = data.sprites.front_default;
    image.src = src;
    $('#imagenPokemon').append(image);	
}

Pokedex.prototype.paintDescription=function(data){
	var self = this;

	var descripciones = []

	for(var i=0; i<data.flavor_text_entries.length; i++){
		var name = data.flavor_text_entries[i].language.name
		if(name == "es"){
			descripciones.push(data.flavor_text_entries[i].flavor_text);
		}
	}
	self.descripcion.setValue(descripciones[0])		
}

Pokedex.prototype.pokemon=function(data){
	var self = this;
	this.datos = data;

	this.especies 			= new SelectComponent("#especies");

	for(var i=0; i<this.datos.results.length; i++){

		var item = 	"<div class='col-md-4'>"+
							"<span id="+this.datos.results[i].name+" style='width:90px' class='badge'>"+this.datos.results[i].name+"</span>"+
					"</div>";
		$("#especies").append(item)

		self.asignaColor(this.datos.results[i].name)
	}
}

Pokedex.prototype.asignaColor = function(id){
	switch(id){
		case "fighting":
			$("#fighting").css("background-color", "#B9594E");
		break;
		case "flying":
			$("#flying").css("background-color", "#6C9DEC");
		break;
		case "poison":
			$("#poison").css("background-color", "#6E3D5F");
		break;
		case "ground":
			$("#ground").css("background-color", "#D4BC70");
		break;
		case "rock":
			$("#rock").css("background-color", "#83753D");
		break;
		case "bug":
			$("#bug").css("background-color", "#88942E");
		break;
		case "ghost":
			$("#ghost").css("background-color", "#D06B8D");
		break;
		case "steel":
			$("#steel").css("background-color", "#6B656C");
		break;
		case "fire":
			$("#fire").css("background-color", "#E74926");
		break;
		case "water":
			$("#water").css("background-color", "#3296F9");
		break;
		case "grass":
			$("#grass").css("background-color", "#538B3C");
		break;
		case "electric":
			$("#electric").css("background-color", "#EEC140");
		break;
		case "ice":
			$("#ice").css("background-color", "#7FDAF7");
		break;
		case "dragon":
			$("#dragon").css("background-color", "#554B7E");
		break;
		case "dark":
			$("#dark").css("background-color", "#68574B");
		break;
		case "fairy":
			$("#fairy").css("background-color", "#FBAFF8");
		break;
		case "psychic":
			$("#psychic").css("background-color", "#69669C");
		break;
		case "shadow":
			$("#shadow").css("background-color", "#69669C");
		break;
		case "unknown":
			$("#unknown").css("background-color", "#6CA191");
		break;
		case "normal":
			$("#normal").css("background-color", "#CAC9C5");
		break;
	}
}

Pokedex.prototype.showColapse = function(){
	$('#especies').html("")

	this.servicios.especies()
	.then(this.pokemon.bind(this))

	$("#collapse1").toggle();
	$("#collapse2").toggle();
	$("#buttonsTop").toggle();

	$("#busqueda").show();
	$("#especiesList").show();
	$("#infoPokemon").hide();
	$("#imgPkemon").hide();
	$("#descriptionDiv").hide();

	this.nombrePokemon.value = ""
	this.buttonclose.element.disabled=true

	if ($('#imgPkemon').hasClass('col-md-12')){
        $('#imgPkemon').addClass('col-md-5').removeClass('col-md-12');
        $("#descriptionsPokemon").show();
    }

}

Pokedex.prototype.closeColapse = function(){

	$("#infoPokemon").toggle();

	$("#descriptionsPokemon").toggle();
    if ($('#imgPkemon').hasClass('col-md-12')){
        $('#imgPkemon').addClass('col-md-5').removeClass('col-md-12');
    }else if($('#imgPkemon').hasClass('col-md-5')){
        $('#imgPkemon').addClass('col-md-12').removeClass('col-md-5');
    }
	
	

}

module.exports = Pokedex; 