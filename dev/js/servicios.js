var Servicios = function() {

	this.AjaxType="GET"

} 

Servicios.prototype.todos = function(){

	var myUrl = "https://pokeapi.co/api/v2/";
	return this.ajaxCall(myUrl,this.AjaxType,"application/json");	
}

Servicios.prototype.especies = function(){

	var myUrl = "https://pokeapi.co/api/v2/type/";
	return this.ajaxCall(myUrl,this.AjaxType,"application/json");	
}

Servicios.prototype.nombrePokemon = function(name){
	var myUrl = "https://pokeapi.co/api/v2/pokemon/"+name+"/"
	return this.ajaxCall(myUrl,this.AjaxType,"application/json");
}

Servicios.prototype.idPokemon = function(id){
	var myUrl = "https://pokeapi.co/api/v2/pokemon/"+id+"/"
	return this.ajaxCall(myUrl,this.AjaxType,"application/json");
}

Servicios.prototype.caracteristicPokemon = function(id){
	var myUrl = "https://pokeapi.co/api/v2/pokemon-species/"+id+"/"
	return this.ajaxCall(myUrl,this.AjaxType,"application/json");
}

Servicios.prototype.ajaxCall = function(url,type){
	console.info("Services::ajaxCall() ",url,type);
	  
	
	var deferred = $.Deferred();
	$.ajax ({
	   	type: type,
	   	url: url,
	   	success: function (data){
	   		console.info('success ajaxCall: ',data);
	   		deferred.resolve(data );
	   	},
	   	fail: function (data,status){ 
	   		console.info('fail: ',data,status);
	   		deferred.reject(data); 
	   	},
	   	error: function (data,status){ 
	   		console.info('error: ',data,status)
	   		deferred.reject(data); 
	   	}
	});
	return deferred.promise();
}

module.exports = Servicios;