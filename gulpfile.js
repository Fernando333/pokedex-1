var gulp 	= require('gulp');
connect 	= require('gulp-connect');
sass    	= require('gulp-sass');
uglify 		= require('gulp-uglify');
browserify  = require('gulp-browserify');
sourcemaps  = require('gulp-sourcemaps');	
replace		= require('gulp-replace');
rename		= require('gulp-rename');
	
 
gulp.task('default',['html','sass','js','browserify','img','server','watch','fonts']);

gulp.task('server', function() {
		connect.server({
		root: './public',
		livereload: true,
		port:8085,
	})
});

gulp.task('sass', function () {
  return gulp.src('./dev/scss/pokedex.scss')
	.pipe(sourcemaps.init())
	.pipe(sass().on('error', sass.logError))
	.pipe(sourcemaps.write('./maps'))
	.pipe(gulp.dest('./public/css'))
	.pipe(connect.reload());
});

gulp.task('fonts', function(){
	gulp.src('./dev/fonts/**/**.*')
	.pipe(gulp.dest('./public/fonts'))
	.pipe(connect.reload());
});

gulp.task('html', function(){
	gulp.src('./dev/**/**.html')
	.pipe(gulp.dest('./public'))
	.pipe(connect.reload());
});

gulp.task('js',function(){
  	gulp.src('./dev/js/pokedex.js')
	.pipe(browserify())
	.pipe(gulp.dest('./public/js'))
	.pipe(connect.reload());
});


gulp.task('img', function(){
	gulp.src('./dev/img/**/**.*')
	.pipe(gulp.dest('./public/img'))
	.pipe(connect.reload());
});

gulp.task('browserify', function() {
    return gulp.src('./dev/js/script.min.js')
	.pipe(browserify())
	.pipe(gulp.dest('./public/js'))
	.pipe(connect.reload());
});

gulp.task('watch', function () {
	gulp.watch(['./dev/scss/**/**.scss'], ['sass']);
	gulp.watch(['./dev/js/**/**.js'], ['browserify']);
	gulp.watch(['./dev/**/**.html'], ['html']);
});

